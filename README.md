# EBI MetaG Bioinformatics

Course materials for the comparative metagenomic module of the
2021 EMBL-EBI Metagenomics Bioinformatics course.

Course website: [https://www.ebi.ac.uk/training/materials/metagenomicsbioinformatics2021/](https://www.ebi.ac.uk/training/materials/metagenomicsbioinformatics2021/)
